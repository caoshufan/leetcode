#include <iostream>
#include <climits>
#include <unordered_set>

using namespace std;

class Solution {
public:
	int lengthOfLongestSubstring(string s) {
		unordered_set<char> charSet;
		size_t ans = 0;
		size_t i;
		for(i = 0; i < s.size(); i++) {
			charSet.insert(s.at(i));
			size_t j;
			for (j = i + 1; j < s.size(); j++){
				if (charSet.find(s.at(j)) != charSet.end()) {
					break;
				}else {
					charSet.insert(s.at(j));
				}
			}
			if (ans < charSet.size()) {
				ans = charSet.size();
				if(j == s.size())
					return ans;
			}
			charSet.clear();
		}
		return ans;
	}
};
int main(int argc, char *argv[]) {
	
}