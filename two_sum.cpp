#include <iostream>

using namespace std;

class Solution {
public:
	vector<int> twoSum(vector<int>& nums, int target) {
		// O(n^2)
//		vector<int> ans;
//		bool flag = false;
//		
//		for(int i = 0; i < nums.size(); i++){
//			
//			for(int j = i + 1; j < nums.size(); j++) {
//				if(nums[i] + nums[j] == target) {
//					ans.push_back(i);
//					ans.push_back(j);
//					flag = true;
//					break;
//				}
//			} 
//			if(flag){
//				break;
//			}
//		}
//		return ans;
		
		//O(n)
		unordered_map<int,int> hash;
		vector<int> ans;
		for(int i = 0; i < nums.size(); i++) {
			auto p = hash.find(target - nums[i]);
			if(p != hash.end()) {
				return {p->second + 1, i + 1};
			}
			hash[nums[i]] = i;
		}
	}
};

int main(int argc, char *argv[]) {
	
}