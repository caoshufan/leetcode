#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;


void func(int *e,void** data) {
	*data = e;
}

void func1(void *data) {
	printf("%d",*(int*)data);
}

int main(int argc, char *argv[]) {
	int a = 1;
	int *c = &a;
//	c = &a;
	void *data;
	func(c,(void**)&data);
	printf("%d",*(int*)data);
	func1(&a);
}