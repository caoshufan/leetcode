#include <iostream>

using namespace std;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
	ListNode* swapPairs(ListNode* head) {
		ListNode** node = &head;
		ListNode* first;
		ListNode* second;
		
		while ((first = *node) && (second = first->next) ) {
			first->next = second->next;
			second->next = first;
			*node = second;
			node = &(first->next);    
		}
		return head;
	}
};

int main(int argc, char *argv[]) {
	
}